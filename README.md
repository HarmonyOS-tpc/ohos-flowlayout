# ohos-flowlayout

ohos-flowlayout : ohos-flowlayout, wrap its content to the next line if there is no space in the current line.

# Usage Instructions
1.Change flayout:debugDraw attribute to true/false for draw lines.
2.Change flayout:layoutDirection attribute to ltr/rtl for layoutDirection change with horizontal orientation.
3.Change flayout:orientation attribute to horizontal/vertical for orientation change.

Add the `org.apmem.tools.layout.FlowLayout` to your layout XML file.

```
<?xml version="1.0" encoding="utf-8"?>
<org.apmem.tools.layouts.FlowLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    xmlns:flayout="http://schemas.huawei.com/hap/res-auto"
    ohos:id="$+id:flowLayout"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:background_element="$graphic:background_ability_main"
    ohos:bottom_padding="12vp"
    ohos:left_padding="6vp"
    ohos:right_padding="12vp"
    ohos:top_padding="6vp"
    flayout:debugDraw="true"
    flayout:layoutDirection="ltr"
    flayout:orientation="horizontal">

    <Button
        ohos:height="30vp"
        ohos:width="120vp"
        ohos:background_element="$graphic:background_button"
        ohos:text="$string:test_button_one"
        ohos:text_color="$color:material_white"/>

    <Button
        ohos:height="50vp"
        ohos:width="100vp"
        ohos:background_element="$graphic:background_button"
        ohos:text="$string:test_button_two"
        ohos:text_color="$color:material_white"/>

    <Button
        ohos:height="40vp"
        ohos:width="140vp"
        ohos:background_element="$graphic:background_button"
        ohos:text="$string:test_button_three"
        ohos:text_color="$color:material_white"/>

    <Button
        ohos:height="20vp"
        ohos:width="110vp"
        ohos:background_element="$graphic:background_button"
        ohos:text="$string:test_button_four"
        ohos:text_color="$color:material_white"/>

    <Button
        ohos:height="50vp"
        ohos:width="120vp"
        ohos:background_element="$graphic:background_button"
        ohos:text="$string:test_button_five"
        ohos:text_color="$color:material_white"/>

    <Button
        ohos:height="50vp"
        ohos:width="100vp"
        ohos:background_element="$graphic:background_button"
        ohos:text="$string:test_button_six"
        ohos:text_color="$color:material_white"/>

    <Button
        ohos:height="30vp"
        ohos:width="90vp"
        ohos:background_element="$graphic:background_button"
        ohos:text="$string:test_button_seven"
        ohos:text_color="$color:material_white"/>

    <Button
        ohos:height="40vp"
        ohos:width="70vp"
        ohos:background_element="$graphic:background_button"
        ohos:text="$string:test_button_eight"
        ohos:text_color="$color:material_white"/>

    <Button
        ohos:height="50vp"
        ohos:width="90vp"
        ohos:background_element="$graphic:background_button"
        ohos:text="$string:test_button_nine"
        ohos:text_color="$color:material_white"/>

    <Button
        ohos:height="90vp"
        ohos:width="90vp"
        ohos:background_element="$graphic:background_button"
        ohos:text="$string:test_button_ten"
        ohos:text_color="$color:material_white"/>

    <Button
        ohos:height="70vp"
        ohos:width="60vp"
        ohos:background_element="$graphic:background_button"
        ohos:text="$string:test_button_eleven"
        ohos:text_color="$color:material_white"/>

    <Button
        ohos:height="40vp"
        ohos:width="50vp"
        ohos:background_element="$graphic:background_button"
        ohos:text="$string:test_button_twelve"
        ohos:text_color="$color:material_white"/>

    <Button
        ohos:height="30vp"
        ohos:width="120vp"
        ohos:background_element="$graphic:background_button"
        ohos:text="$string:test_button_thirteen"
        ohos:text_color="$color:material_white"/>

    <Text
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="$string:test_text"
        ohos:text_color="#FF2D00"/>

</org.apmem.tools.layouts.FlowLayout>
```

# Installation Instructions
1.For using ohos-flowlayout module in sample application,include the below library dependency to generate hap/layout.har.

Modify entry build.gradle as below :
```
dependencies {
    implementation project(path: ':layouts')
}
```

2.For using ohos-flowlayout in separate application, make sure to add the "layouts.har" in libs folder of "entry" module.

Modify entry build.gradle as below :

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.har'])
}
```

3.For using ohos-flowlayout from remote repository in separate application, add the below dependency in "entry" build.gradle.

Modify entry build.gradle as below :

```
dependencies {
    implementation 'io.openharmony.tpc.thirdlib:ohos-flowlayout:1.0.1'
}
```

# Copyrights
Copyright 2011, Artem Votincev (apmem.org)

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
```
   http://www.apache.org/licenses/LICENSE-2.0
```
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.See the License for the specific language governing permissions and limitations under the License.