/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apmem.tools.layouts;

/**
 * class Constants
 */
class Constants {
    /**
     * DEFAULT_MARGIN
     */
    static final int DEFAULT_MARGIN = 10;

    /**
     * DEFAULT_MARGIN_RIGHT
     */
    static final int DEFAULT_MARGIN_RIGHT = 40;

    /**
     * COORDINATE_INCREMENT
     */
    static final int COORDINATE_INCREMENT = 15;

    /**
     * COLOUR_GREEN
     */
    static final int COLOUR_GREEN = 0xff00ff00;

    /**
     * FLOAT_DIVIDER
     */
    static final float FLOAT_DIVIDER = 2.0f;

    /**
     * LINE_WIDTH
     */
    static final float LINE_WIDTH = 5.0f;

    private Constants() {
        /* Do nothing */
    }
}

