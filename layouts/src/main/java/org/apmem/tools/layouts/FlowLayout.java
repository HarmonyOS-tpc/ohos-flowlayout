package org.apmem.tools.layouts;

import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Component;
import ohos.agp.components.AttrSet;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;

import ohos.app.Context;

import static org.apmem.tools.layouts.Constants.DEFAULT_MARGIN_RIGHT;
import static org.apmem.tools.layouts.Constants.DEFAULT_MARGIN;
import static org.apmem.tools.layouts.Constants.COORDINATE_INCREMENT;
import static org.apmem.tools.layouts.Constants.COLOUR_GREEN;
import static org.apmem.tools.layouts.Constants.FLOAT_DIVIDER;
import static org.apmem.tools.layouts.Constants.LINE_WIDTH;

/**
 * Function description
 * class FlowLayout
 */
public class FlowLayout extends ComponentContainer implements Component.DrawTask {
    private String mOrientation = "orientation";

    private String mDebugDraw = "debugDraw";

    private String mLayoutDirection = "layoutDirection";

    private AttrSet attrSet;

    private boolean mIsDebugDrawValue = false;

    private String mLayoutDirectionValue = getLayoutDirection().toString();

    private String mOrientationValue;

    private int maxRowHeight = 0;

    private int maxColumnWidth = 0;

    /**
     * Constructor
     *
     * @param context Context
     */
    public FlowLayout(Context context) {
        this(context, null);
        init();
        readStyleParameters(attrSet);
    }

    /**
     * Constructor
     *
     * @param context Context
     * @param attrSet Attrset
     */
    public FlowLayout(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
        this.attrSet = attrSet;
        init();
        readStyleParameters(attrSet);
    }

    /**
     * Constructor
     *
     * @param context   Context
     * @param attrSet   Attrset
     * @param styleName String
     */
    public FlowLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        this.attrSet = attrSet;
        init();
        readStyleParameters(attrSet);
    }

    private void init() {
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        onLayout(canvas);
    }

    private void readStyleParameters(AttrSet set) {
        mIsDebugDrawValue = set.getAttr(mDebugDraw).isPresent() && set.getAttr(mDebugDraw).get().getBoolValue();
        mLayoutDirectionValue = set.getAttr(mLayoutDirection).isPresent() ? set.getAttr(mLayoutDirection)
                .get()
                .getStringValue() : "RTL";
        mOrientationValue = set.getAttr(mOrientation).isPresent() ? set.getAttr(mOrientation)
                .get()
                .getStringValue() : "horizontal";
    }

    private void onLayout(Canvas canvas) {
        if (mOrientationValue.equalsIgnoreCase("horizontal")) {
            if (mLayoutDirectionValue.equalsIgnoreCase("ltr")) {
                setLayoutDirection(LayoutDirection.LTR);
                showLeftToRightView(canvas);
            } else {
                setLayoutDirection(LayoutDirection.RTL);
                showRightToLeftView(canvas);
            }
        } else {
            setVerticalLayout(canvas);
        }
    }

    private void showRightToLeftView(Canvas canvas) {
        float mRtlXpointer = getWidth() - getPaddingLeft() - getPaddingRight();
        float mRtlYpointer = getContentPositionY();
        int remainingWidth = getWidth() - getPaddingLeft() - getPaddingRight() - DEFAULT_MARGIN;

        for (int childIndex = 0; childIndex < getChildCount(); childIndex++) {
            Component child = getComponentAt(childIndex);
            int componentWidth = child.getWidth();
            if (childIndex > 0) {
                if ((componentWidth + DEFAULT_MARGIN_RIGHT + child.getMarginRight() + getMarginLeft()) <
                        (remainingWidth - getPaddingLeft() - getPaddingRight() - DEFAULT_MARGIN_RIGHT)) {
                    child.setTranslation(mRtlXpointer - child.getWidth() - DEFAULT_MARGIN, mRtlYpointer);
                    if (mIsDebugDrawValue) {
                        drawDebugInfoRtl(child, canvas, mRtlXpointer - child.getWidth() - DEFAULT_MARGIN, mRtlYpointer);
                    }
                    setRowMaximumHeight(child.getHeight());
                    mRtlXpointer = mRtlXpointer - (child.getWidth() + DEFAULT_MARGIN + child.getMarginLeft() + child.getMarginRight());
                    remainingWidth -= child.getWidth() - child.getMarginLeft() - getMarginRight() - DEFAULT_MARGIN_RIGHT;
                } else {
                    remainingWidth = getWidth() - getPaddingLeft() - getPaddingRight() - DEFAULT_MARGIN_RIGHT;
                    mRtlXpointer = getWidth() - getPaddingLeft() - getPaddingRight();
                    mRtlYpointer += maxRowHeight + DEFAULT_MARGIN;
                    maxRowHeight = DEFAULT_MARGIN + child.getHeight() + child.getMarginTop() + child.getMarginBottom();
                    child.setTranslation(mRtlXpointer - child.getWidth() - child.getMarginLeft() - getMarginRight(), mRtlYpointer);
                    if (mIsDebugDrawValue) {
                        drawDebugInfoRtl(child, canvas, mRtlXpointer - child.getWidth() - child.getMarginLeft() - getMarginRight(), mRtlYpointer);
                    }
                    mRtlXpointer = mRtlXpointer - (child.getWidth() + child.getMarginLeft() + child.getMarginRight());
                    remainingWidth -= child.getWidth() - child.getMarginLeft() - getMarginRight() - DEFAULT_MARGIN_RIGHT;
                }
            } else {
                child.setTranslation(mRtlXpointer - child.getWidth(), mRtlYpointer);
                if (mIsDebugDrawValue) {
                    drawDebugInfoRtl(child, canvas, mRtlXpointer - child.getWidth(), mRtlYpointer);
                }
                maxRowHeight = DEFAULT_MARGIN + child.getHeight() + child.getMarginTop() + child.getMarginBottom();
                mRtlXpointer = mRtlXpointer - (child.getWidth() + child.getMarginLeft() + child.getMarginRight());
                remainingWidth -= child.getWidth() - getPaddingLeft() - getPaddingRight() - child.getMarginLeft() - child.getMarginRight();
            }
        }
    }

    private void showLeftToRightView(Canvas canvas) {
        float mXpointer = getContentPositionX();
        float mYpointer = getContentPositionY();
        int remainingWidth = getWidth() - getPaddingLeft() - getPaddingRight() - DEFAULT_MARGIN;
        for (int childIndex = 0; childIndex < getChildCount(); childIndex++) {
            Component child = getComponentAt(childIndex);
            int componentWidth = child.getWidth();
            if (childIndex > 0) {
                if (((componentWidth + DEFAULT_MARGIN_RIGHT + child.getMarginRight() + getMarginLeft()) <
                        (remainingWidth - getPaddingLeft() - getPaddingRight() - DEFAULT_MARGIN_RIGHT))) {
                    child.setTranslation(mXpointer, mYpointer);
                    if (mIsDebugDrawValue) {
                        drawDebugInfo(child, canvas, mXpointer, mYpointer);
                    }
                    setRowMaximumHeight(child.getHeight());
                    mXpointer += DEFAULT_MARGIN + child.getWidth() + child.getMarginLeft() + child.getMarginRight();
                    remainingWidth -= child.getWidth() - child.getMarginLeft() - getMarginRight() - DEFAULT_MARGIN_RIGHT;
                } else {
                    remainingWidth = getWidth() - getPaddingLeft() - getPaddingRight() - DEFAULT_MARGIN_RIGHT;
                    mXpointer = getContentPositionX();
                    mYpointer += maxRowHeight + DEFAULT_MARGIN;
                    maxRowHeight = DEFAULT_MARGIN + child.getHeight() + child.getMarginTop() + child.getMarginBottom();
                    child.setTranslation(mXpointer, mYpointer);
                    if (mIsDebugDrawValue) {
                        drawDebugInfo(child, canvas, mXpointer, mYpointer);
                    }
                    mXpointer += child.getWidth() + child.getMarginLeft() + child.getMarginRight() + DEFAULT_MARGIN;
                    remainingWidth -= child.getWidth() - child.getMarginLeft() - getMarginRight() - DEFAULT_MARGIN_RIGHT;
                }
            } else {
                child.setTranslation(mXpointer, mYpointer);
                if (mIsDebugDrawValue) {
                    drawDebugInfo(child, canvas, mXpointer, mYpointer);
                }
                maxRowHeight = DEFAULT_MARGIN + child.getHeight() + child.getMarginTop() + child.getMarginBottom();
                mXpointer += DEFAULT_MARGIN + child.getWidth() + child.getMarginLeft() + child.getMarginRight();
                remainingWidth -= child.getWidth() - getPaddingLeft() - getPaddingRight() - child.getMarginLeft() - child.getMarginRight();
            }
        }
    }

    private void setRowMaximumHeight(int childHeight) {
        if (childHeight > maxRowHeight) {
            maxRowHeight = childHeight + DEFAULT_MARGIN;
        }
    }

    private void setColumnMaximumWidth(int childWidth) {
        if (childWidth > maxColumnWidth) {
            maxColumnWidth = childWidth + DEFAULT_MARGIN;
        }
    }

    private void setVerticalLayout(Canvas canvas) {
        float mVerticalXpointer = getContentPositionX();
        float mVerticalYpointer = getContentPositionY();
        int remainingHeight = getHeight();
        for (int childIndex = 0; childIndex < getChildCount(); childIndex++) {
            Component child = getComponentAt(childIndex);
            int componentHeight = child.getHeight();
            if (childIndex > 0) {
                remainingHeight -= child.getHeight() - child.getMarginTop() - getMarginBottom() - DEFAULT_MARGIN;
                if (((componentHeight + DEFAULT_MARGIN + child.getMarginTop() + getMarginBottom()) <
                        (remainingHeight - getPaddingTop() - getPaddingBottom() - DEFAULT_MARGIN_RIGHT))) {
                    child.setTranslation(mVerticalXpointer, mVerticalYpointer + COORDINATE_INCREMENT);
                    if (mIsDebugDrawValue) {
                        drawDebugInfo(child, canvas, mVerticalXpointer, mVerticalYpointer + COORDINATE_INCREMENT);
                    }
                    setColumnMaximumWidth(child.getWidth());
                    mVerticalYpointer += DEFAULT_MARGIN + child.getHeight() + child.getMarginTop() + child.getMarginBottom();
                } else {
                    remainingHeight = getHeight();
                    mVerticalYpointer = 0;
                    mVerticalXpointer += maxColumnWidth + DEFAULT_MARGIN;
                    maxColumnWidth = DEFAULT_MARGIN + child.getWidth() + child.getMarginLeft() + child.getMarginRight();
                    child.setTranslation(mVerticalXpointer, mVerticalYpointer);
                    if (mIsDebugDrawValue) {
                        drawDebugInfo(child, canvas, mVerticalXpointer, mVerticalYpointer);
                    }
                    mVerticalYpointer += child.getHeight() + child.getMarginTop() + child.getMarginBottom();
                }
            } else {
                child.setTranslation(mVerticalXpointer, mVerticalYpointer);
                if (mIsDebugDrawValue) {
                    drawDebugInfo(child, canvas, mVerticalXpointer, mVerticalYpointer);
                }
                maxColumnWidth = DEFAULT_MARGIN + child.getWidth() + child.getMarginLeft() + child.getMarginRight();
                mVerticalYpointer += child.getHeight() + child.getMarginTop() + child.getMarginBottom();
                remainingHeight -= child.getHeight() - child.getMarginTop() - child.getMarginBottom();
            }
        }
    }

    private void drawDebugInfo(Component child, Canvas canvas, float horizontal, float vertical) {
        float verticalLineX = horizontal + (child.getWidth() / FLOAT_DIVIDER) + child.getMarginLeft();
        float verticalLineY = vertical + child.getTop() + child.getHeight() + COORDINATE_INCREMENT;
        Point verticalLineStartPoint = new Point(verticalLineX, verticalLineY);
        Point verticalLineEndPoint = new Point(verticalLineX, verticalLineY + COORDINATE_INCREMENT);
        canvas.drawLine(verticalLineStartPoint, verticalLineEndPoint, createPaint());

        float horizontalLineX = horizontal + child.getRight() + COORDINATE_INCREMENT;
        float horizontalLineY = vertical + child.getTop() + (child.getHeight() / FLOAT_DIVIDER) + DEFAULT_MARGIN;
        Point horizontalStartPoint = new Point(horizontalLineX, horizontalLineY);
        Point horizontalEndPoint = new Point(horizontalLineX + COORDINATE_INCREMENT, horizontalLineY);
        canvas.drawLine(horizontalStartPoint, horizontalEndPoint, createPaint());
    }

    private void drawDebugInfoRtl(Component child, Canvas canvas, float horizontal, float vertical) {
        float verticalLineX = horizontal + (child.getWidth() / FLOAT_DIVIDER) + child.getMarginLeft();
        float verticalLineY = vertical + child.getTop() + child.getHeight() + COORDINATE_INCREMENT;
        Point verticalLineStartPoint = new Point(verticalLineX, verticalLineY);
        Point verticalLineEndPoint = new Point(verticalLineX, verticalLineY + COORDINATE_INCREMENT);
        canvas.drawLine(verticalLineStartPoint, verticalLineEndPoint, createPaint());

        float horizontalLineX = horizontal + child.getLeft() + COORDINATE_INCREMENT;
        float horizontalLineY = vertical + child.getTop() + (child.getHeight() / FLOAT_DIVIDER) + DEFAULT_MARGIN;
        Point horizontalStartPoint = new Point(horizontalLineX, horizontalLineY);
        Point horizontalEndPoint = new Point(horizontalLineX - COORDINATE_INCREMENT, horizontalLineY);
        canvas.drawLine(horizontalStartPoint, horizontalEndPoint, createPaint());
    }

    private Paint createPaint() {
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE_STYLE);
        paint.setAntiAlias(true);
        paint.setColor(new Color(COLOUR_GREEN));
        paint.setStrokeWidth(LINE_WIDTH);
        return paint;
    }
}
