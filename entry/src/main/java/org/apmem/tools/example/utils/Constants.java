/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apmem.tools.example.utils;

/**
 * class Constants
 */
public class Constants {
    /**
     * LEFT_TO_RIGHT
     */
    public static final String LEFT_TO_RIGHT = "action.FlowLayoutLeftToRightSlice";

    /**
     * LEFT_TO_RIGHT_NO_DEBUG_SLICE
     */
    public static final String LEFT_TO_RIGHT_NO_DEBUG_SLICE = "action.FlowLayoutLeftToRightNoDebugSlice";

    /**
     * RIGHT_TO_LEFT
     */
    public static final String RIGHT_TO_LEFT = "action.FlowLayoutRightToLeftSlice";

    /**
     * RIGHT_TO_LEFT_NO_DEBUG_SLICE
     */
    public static final String RIGHT_TO_LEFT_NO_DEBUG_SLICE = "action.FlowLayoutRightToLeftNoDebugSlice";

    /**
     * VerticalSlice
     */
    public static final String VERTICAL_SLICE = "action.FlowLayoutVerticalSlice";

    private Constants() {
        /* Do nothing */
    }
}

