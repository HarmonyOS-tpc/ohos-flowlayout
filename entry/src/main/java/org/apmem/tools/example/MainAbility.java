/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apmem.tools.example;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

import org.apmem.tools.example.slice.MainAbilitySlice;
import org.apmem.tools.example.slice.FlowLayoutLeftToRightSlice;
import org.apmem.tools.example.slice.FlowLayoutLeftToRightNoDebugSlice;
import org.apmem.tools.example.slice.FlowLayoutRightToLeftSlice;
import org.apmem.tools.example.slice.FlowLayoutRightToLeftNoDebugSlice;
import org.apmem.tools.example.slice.FlowLayoutVerticalSlice;

import static org.apmem.tools.example.utils.Constants.LEFT_TO_RIGHT;
import static org.apmem.tools.example.utils.Constants.LEFT_TO_RIGHT_NO_DEBUG_SLICE;
import static org.apmem.tools.example.utils.Constants.RIGHT_TO_LEFT;
import static org.apmem.tools.example.utils.Constants.RIGHT_TO_LEFT_NO_DEBUG_SLICE;
import static org.apmem.tools.example.utils.Constants.VERTICAL_SLICE;

/**
 * Function description
 * class MainAbility
 */
public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());

        addActionRoute(LEFT_TO_RIGHT, FlowLayoutLeftToRightSlice.class.getName());
        addActionRoute(LEFT_TO_RIGHT_NO_DEBUG_SLICE, FlowLayoutLeftToRightNoDebugSlice.class.getName());
        addActionRoute(RIGHT_TO_LEFT, FlowLayoutRightToLeftSlice.class.getName());
        addActionRoute(RIGHT_TO_LEFT_NO_DEBUG_SLICE, FlowLayoutRightToLeftNoDebugSlice.class.getName());
        addActionRoute(VERTICAL_SLICE, FlowLayoutVerticalSlice.class.getName());
    }
}
