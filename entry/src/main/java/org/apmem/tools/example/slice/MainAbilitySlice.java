/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apmem.tools.example.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

import ohos.agp.components.Button;
import ohos.agp.components.Component;

import org.apmem.tools.example.MainAbility;
import org.apmem.tools.example.ResourceTable;

import static org.apmem.tools.example.utils.Constants.LEFT_TO_RIGHT;
import static org.apmem.tools.example.utils.Constants.LEFT_TO_RIGHT_NO_DEBUG_SLICE;
import static org.apmem.tools.example.utils.Constants.RIGHT_TO_LEFT;
import static org.apmem.tools.example.utils.Constants.RIGHT_TO_LEFT_NO_DEBUG_SLICE;
import static org.apmem.tools.example.utils.Constants.VERTICAL_SLICE;

/**
 * Function description
 * class MainAbilitySlice
 */
public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        initialiseViews(intent);
    }

    private void initialiseViews(Intent intent) {
        Component buttonComponent = findComponentById(ResourceTable.Id_flow_layout_ltr);
        if (buttonComponent instanceof Button) {
            Button flowLayoutLtr = (Button) buttonComponent;
            flowLayoutLtr.setClickedListener(component -> viewFlowLayoutLtr(intent));
        }

        buttonComponent = findComponentById(ResourceTable.Id_flow_layout_ltr_no_debug_info);
        if (buttonComponent instanceof Button) {
            Button flowLayoutLtrNoDebugInfo = (Button) buttonComponent;
            flowLayoutLtrNoDebugInfo.setClickedListener(component -> viewFlowLayoutLtrNoDebugInfo(intent));
        }

        buttonComponent = findComponentById(ResourceTable.Id_flow_layout_rtl);
        if (buttonComponent instanceof Button) {
            Button flowLayoutRtl = (Button) buttonComponent;
            flowLayoutRtl.setClickedListener(component -> viewFlowLayoutRtl(intent));
        }

        buttonComponent = findComponentById(ResourceTable.Id_flow_layout_rtl_no_debug_info);
        if (buttonComponent instanceof Button) {
            Button flowLayoutRtlNoDebugInfo = (Button) buttonComponent;
            flowLayoutRtlNoDebugInfo.setClickedListener(component -> viewFlowLayoutRtlNoDebugInfo(intent));
        }

        buttonComponent = findComponentById(ResourceTable.Id_flow_layout_vertical);
        if (buttonComponent instanceof Button) {
            Button flowLayoutVertical = (Button) buttonComponent;
            flowLayoutVertical.setClickedListener(component -> viewFlowLayoutVertical(intent));
        }
    }

    private void viewFlowLayoutLtr(Intent intent) {
        Operation flowLayoutOperation = new Intent.OperationBuilder().withAction(LEFT_TO_RIGHT)
                .withBundleName(getBundleName())
                .withAbilityName(MainAbility.class.getSimpleName())
                .build();
        intent.setOperation(flowLayoutOperation);
        startAbility(intent);
    }

    private void viewFlowLayoutLtrNoDebugInfo(Intent intent) {
        Operation ltrNoDebugOperation = new Intent.OperationBuilder().withAction(LEFT_TO_RIGHT_NO_DEBUG_SLICE)
                .withBundleName(getBundleName())
                .withAbilityName(MainAbility.class.getSimpleName())
                .build();
        intent.setOperation(ltrNoDebugOperation);
        startAbility(intent);
    }

    private void viewFlowLayoutRtl(Intent intent) {
        Operation flowLayoutRtlOperation = new Intent.OperationBuilder().withAction(
                RIGHT_TO_LEFT)
                .withBundleName(getBundleName())
                .withAbilityName(MainAbility.class.getSimpleName())
                .build();
        intent.setOperation(flowLayoutRtlOperation);
        startAbility(intent);
    }

    private void viewFlowLayoutRtlNoDebugInfo(Intent intent) {
        Operation rtlNoDebugOperation = new Intent.OperationBuilder().withAction(RIGHT_TO_LEFT_NO_DEBUG_SLICE)
                .withBundleName(getBundleName())
                .withAbilityName(MainAbility.class.getSimpleName())
                .build();
        intent.setOperation(rtlNoDebugOperation);
        startAbility(intent);
    }

    private void viewFlowLayoutVertical(Intent intent) {
        Operation flowLayoutVerticalOperation = new Intent.OperationBuilder().withAction(VERTICAL_SLICE)
                .withBundleName(getBundleName())
                .withAbilityName(MainAbility.class.getSimpleName())
                .build();
        intent.setOperation(flowLayoutVerticalOperation);
        startAbility(intent);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
